import React, { useRef, useState } from "react";
import { clone } from 'lodash';
import GSTC, { GSTCResult } from "gantt-schedule-timeline-calendar";
import "gantt-schedule-timeline-calendar/dist/style.css";

// Configuration object
const config = {
  licenseKey: 'X',

  list: {
    columns: {
      data: GSTC.api.fromArray([
        {
          id: 'id',
          label: 'ID',
          data: (item: any) => GSTC.api.sourceID(item.row.id), // show original id (not internal GSTCID)
          width: 80,
          header: {
            content: 'ID',
          },
        },
        {
          id: 'label',
          data: 'label',
          sortable: 'label',
          isHTML: false,
          width: 230,
          header: {
            content: 'Label',
          },
        },
      ]),
    },
    rows: GSTC.api.fromArray([
      {
        id: '1',
        label: 'Row 1',
      },
      {
        id: '2',
        label: 'Row 2',
      },
    ]),
  },
  chart: {
    items: GSTC.api.fromArray([
      {
        id: '3',
        label: 'Item 1',
        rowId: '1',
        time: {
          start: GSTC.api.date('2020-01-01').startOf('day').valueOf(),
          end: GSTC.api.date('2020-01-02').endOf('day').valueOf(),
        },
      },
      {
        id: '4',
        label: 'Item 2',
        rowId: '2',
        time: {
          start: GSTC.api.date('2020-01-15').startOf('day').valueOf(),
          end: GSTC.api.date('2020-01-20').endOf('day').valueOf(),
        },
      },
    ]),
  },
};

// Generate GSTC state from configuration object
const state = GSTC.api.stateFromConfig(config);

export type CalendarWrapperProps = {};

const CalendarWrapper: React.FC<CalendarWrapperProps> = () => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [gstcApp, setApp] = useState<GSTCResult|null>(null);

  const initApp = () => {
    if (!wrapperRef.current) return;
    // @ts-ignore
    const app = GSTC({
      element: wrapperRef.current,
      state,
    });

    setApp(app);
  };

  React.useEffect(() => {
    if (!wrapperRef.current || !!gstcApp) {
      return;
    }

    initApp();
  }, [state, gstcApp]);

  React.useEffect(() => {
    return () => {
      if (!!gstcApp) {
        // @ts-ignore
        gstcApp?.app?.destroy();
      }
    };
  }, [gstcApp]);

  const removeFirstRow = React.useCallback(() => {
    state.update('config', (config: any) => {
      config.list.rows = GSTC.api.fromArray([
        {
          id: '2',
          label: 'Row 2',
        },
      ]);
      config.chart.items = GSTC.api.fromArray([
        {
          id: '4',
          label: 'Item 2',
          rowId: '2',
          time: {
            start: GSTC.api.date('2020-01-15').startOf('day').valueOf(),
            end: GSTC.api.date('2020-01-20').endOf('day').valueOf(),
          },
        },
      ]);
      return config;
    })
  }, [state]);

  const removeSecondRow = React.useCallback(() => {
    state.update('config', (config: any) => {
      config.list.rows = GSTC.api.fromArray([
        {
          id: '1',
          label: 'Row 1',
        },
      ]);
      config.chart.items = GSTC.api.fromArray([
        {
          id: '3',
          label: 'Item 1',
          rowId: '1',
          time: {
            start: GSTC.api.date('2020-01-01').startOf('day').valueOf(),
            end: GSTC.api.date('2020-01-02').endOf('day').valueOf(),
          },
        },
      ]);
      return config;
    })
  }, [state]);

  return (<>
      <div ref={wrapperRef} className="calendar"></div>
      <button onClick={removeFirstRow}>Remove first row</button>
      <button onClick={removeSecondRow}>Remove second row</button>
  </>
  );
};

export default CalendarWrapper;
