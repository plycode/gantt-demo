import React from 'react';
import logo from './logo.svg';
import './App.css';
import CalendarWrapper from './components/CalendarWrapper';


function App() {
  return (
    <div className="App">
      <CalendarWrapper />
    </div>
  );
}

export default App;
